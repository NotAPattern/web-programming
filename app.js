const express = require('express');
const app = express();
const path = require('path');
const router = express.Router();

router.get('/',function(req,res){
  res.sendFile(path.join(__dirname+ '/view' + '/index.html'));
  //__dirname : It will resolve to your project folder.
});

//add the router
app.use('/', router);
app.use(express.static('public'));

app.use('/scripts', express.static(__dirname + '/node_modules/jquery/dist/'));
app.use('/scripts', express.static(__dirname + '/node_modules/chart.js/dist/'));

// Connect to postgesql
const pg   = require('pg');

const config = {
		host: '127.0.0.1',
	    user: 'postgres',
	    database: 'db',
	    password: '',
	    port: 5432
};

const pool = new pg.Pool(config);



app.get('/insert_data', (req, res, next) => {
   pool.connect(function (err, client, done) {
       if (err) {
           console.log("Can not connect to the DB" + err);
       }
	   console.log(req.query);
	   var time = new Date(Date.now());
	   console.log(time);
	   // var insert_data = 'gen_random_uuid(), $1, DATE \'2020-11-30\' + TIME \'$2\', $3, $4'
       client.query('INSERT INTO monitoringdata (uid, varname, datetime, valuenum, valuetext) VALUES (gen_random_uuid(), $1, to_timestamp('+ (time / 1000.0) + '), $2, $3);', [req.query.type, req.query.val, req.query.valuetext], function (err, result) {
            if (err) {
                console.log(err);
                res.status(400).send(err);
            }
            res.status(200).send();
			done();
       })
   })
});

app.get('/get_data_log', (req, res, next) => {
   pool.connect(function (err, client, done) {
       if (err) {
           console.log("Can not connect to the DB" + err);
       }
       client.query('SELECT VALUETEXT FROM monitoringdata ORDER BY DATETIME', function (err, result) {
            if (err) {
                console.log(err);
                res.status(400).send(err);
            }
			done();
			var data = [];
			for(let i = 0; i < result.rows.length; i++){
				if(result.rows[i].valuetext != null){
					data[i] = result.rows[i].valuetext;
				}
			}
		    // result.rows
			data = data.filter(function(x) {
				return x !== undefined && x !== null && x !== "NULL"; 
			});
            res.status(200).send(data);
       })
   })
});

app.get('/get_data/:varname', (req, res, next) => {
   pool.connect(function (err, client, done) {
       if (err) {
           console.log("Can not connect to the DB" + err);
       }
       client.query('SELECT VALUENUM, VALUETEXT FROM monitoringdata WHERE VARNAME=$1 ORDER BY DATETIME', [req.params.varname], function (err, result) {
            if (err) {
                console.log(err);
                res.status(400).send(err);
            }
			done();
			var data = [];
			for(let i = 0; i < result.rows.length; i++){
				data[i] = result.rows[i].valuenum;
			}
		    // result.rows
            res.status(200).send(data);
       })
   })
});
app.get('/del_data', (req, res, next) => {
   pool.connect(function (err, client, done) {
       if (err) {
           console.log("Can not connect to the DB" + err);
       }
       client.query('DELETE FROM monitoringdata', function (err, result) {
            if (err) {
                console.log(err);
                res.status(400).send(err);
            }
            res.status(200).send(result.rows);
       })
   })
});

app.listen(process.env.port || 3000);

console.log('Running at Port 3000');
